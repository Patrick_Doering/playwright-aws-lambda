package de.aws.example;


import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class AppTest {

    @Test
    void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("App should return a greeting", classUnderTest.handleRequest("Hello", null));
    }
}
