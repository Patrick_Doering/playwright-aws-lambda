package de.aws.example;


import static org.junit.jupiter.api.Assertions.assertEquals;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class DemoTest implements RequestHandler<Void, String> {

    @ParameterizedTest(name = "MyFirstPlaywrightDemo")
    @Disabled
    void playwrightDemo() throws InterruptedException {
        System.out.println("Start the test");
        Browser browser = Playwright
                .create()
                .chromium()
                .launch(new BrowserType.LaunchOptions().setHeadless(true));

        Page page = browser.newPage();
        page.navigate("https://github.com/microsoft/playwright-java");

        assertEquals("GitHub - microsoft/playwright-java: Java version of the Playwright testing and automation library", page.title());


        Thread.sleep(7000);
        System.out.println("Ready with test");
    }

    @Override
    public String handleRequest(Void input, Context context) {
        return "Hello World! I'm running via AWS lambda";
    }
}
