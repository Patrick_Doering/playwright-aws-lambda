package de.aws.example;

import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;


public class ArgumentsDataProvider implements ArgumentsProvider {

    private TestingData testingData;

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        testingData = new TestingData();
        createTestingData();
        return Stream.of(
                Arguments.of(testingData)
        );
    }

    void createTestingData() {
        testingData.createTestMessage();
    }
}
